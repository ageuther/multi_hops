import numpy as np
from scipy.linalg import expm
# from .samplers import sample_Sz_weighted

class MASHMD():
    def __init__(self, adiab, dt, bisect=0, decoherence_thresh=None, sym=True):
        ''' 
            Worker class for MASH dynamics

            adiab: AdiabaticRepresentation object
            dt: time step
            bisect: number of times to bisect the timestep if a jump is detected
        '''
        self.adiab = adiab
        self.mass = adiab.mass
        self.dt = dt
        self.bisect = bisect
        self.decoherence_thresh = decoherence_thresh
        self.sym = sym

    def set_x(self, x):
        self.x = np.asfarray(x).copy()

    def set_p(self, p):
        self.p = np.asfarray(p).copy()
    
    def set_S(self, S):
        self.S = np.asfarray(S).copy()

    def set_xpS(self, x, p, S):
        self.set_x(x)
        self.set_p(p)
        self.set_S(S)

    def get_active_state(self, S):
        '''
            Returns the active state based on Sz 
        '''
        return int(S[2] > 0)
    
    def step_spins(self, x, p, S, V, F, dt):
        '''
            Propagate the spin variables using the spin Hamiltonian
        '''
        V_z = (V[1] - V[0]) / 2
        d = F[..., 0, 1]
        d_factor = 2 * np.dot(d, p / self.mass)

        H = np.array([[0, -2 * V_z, d_factor], [2 * V_z, 0, 0], [-d_factor, 0, 0]])
        new_S = expm(H * dt) @ S
        
        return new_S

    def ffun(self):
        '''
            Calls the adiabatic representation to calculate forces, potential energy, NACs and eigenvectors
        '''
        self.V, self.G, self.F, self.U = self.adiab.VgFU(self.x)
        self.f = -self.G[..., self.state]

    def pot(self, x, S):
        '''
            Returns the potential energy based on the active state
        '''
        V = self.adiab.potential(x)
        return V[self.get_active_state(S)]
    
    def get_weight_factor(self, S):
        '''
            Returns the population-population weight factor for a given spin
        '''
        return 2 * np.abs(S[...,2])

    def check_jumps(self, prev_xpS, dt):
        '''
            Handles a hop along the trajectory. Bisects the timestep if required (self.bisect > 0) and rescales the momenta accordingly
        '''
        prev_x, prev_p, prev_S = prev_xpS
        prev_state = int(prev_S[2] > 0)

        new_state = int(self.S[2] > 0)
        if self.state != new_state:
            if (dt > self.dt / 2**self.bisect):
                # Divide the timestep in half and try again
                self.x = prev_x
                self.p = prev_p
                self.S = prev_S
                self.state = prev_state

                self.step(dt / 2)
                self.step(dt / 2)
            else:
                # Do the jump
                d = self.F[..., 0, 1]
                d_mw = d / np.sqrt(self.mass)
                p_mw = self.p / np.sqrt(self.mass)
                d_tilde = np.linalg.norm(d_mw)
                pd = np.dot(p_mw, d_mw) / d_tilde    # Projection of the momenta along the NAC
                p_orth = p_mw - pd * d_mw / d_tilde  # Projection of the momenta orthogonal to the NAC

                init_E = np.square(pd) / 2 + self.V[prev_state]
                if init_E >= self.V[self.state]:
                    # Successful hop (rescale the momenta)
                    pd = np.sign(pd) * np.sqrt((init_E - self.V[self.state]) * 2)
                    self.p = np.sqrt(self.mass) * (p_orth + pd * d_mw / d_tilde)
                else:
                    # Unsuccessful hop (reflect the momenta and flip the spin)
                    self.state = prev_state
                    self.p = np.sqrt(self.mass) * (p_orth - pd * d_mw / d_tilde)
                    self.S[2] = -self.S[2]

    # def decoherence_check(self, prev_V):
    #     '''
    #         Checks for decoherence based on the potential energy difference
    #     '''
    #     if self.decoherence_thresh is None:
    #         return
        
    #     prev_diff = np.abs(prev_V[1] - prev_V[0])
    #     curr_diff = np.abs(self.V[1] - self.V[0])

    #     if (curr_diff > self.decoherence_thresh) and (prev_diff < self.decoherence_thresh):
    #         self.S = sample_Sz_weighted(np.sign(self.S[2]), 1)[0]
    #         print('Decoherence detected! Spin resampled.')

    def step(self, dt):
        prev_xpS = [self.x.copy(), self.p.copy(), self.S.copy()]
        
        # Velocity-Verlet
        self.ffun()
        prev_V = self.V.copy() # Store the potential energy for decoherence check

        if self.sym:
            self.S = self.step_spins(self.x, self.p, self.S, self.V, self.F, dt/2)
            # self.state = int(self.S[2] > 0)
            self.check_jumps(prev_xpS, dt)   # Compare current state to previous state and handle jumps

        self.p += self.f * dt / 2
        self.x += self.p * dt / self.mass
        self.ffun()
        self.p += self.f * dt / 2


        self.S = self.step_spins(self.x, self.p, self.S, self.V, self.F, dt/2 if self.sym else dt)
        # self.state = int(self.S[2] > 0)
        self.check_jumps(prev_xpS, dt)   # Compare current state to previous state and handle jumps
        # self.decoherence_check(prev_V)

        return self.x, self.p, self.S
    
    def path(self, TimeSteps, include0=True):
        if include0:
            xt = [self.x.copy()]
            pt = [self.p.copy()]
            St = [self.S.copy()]
        else:
            xt = []
            pt = []
            St = []
        self.state = int(self.S[2] > 0)
        for t in range(int(include0),TimeSteps):
            self.step(self.dt)
            xt.append(self.x.copy())
            pt.append(self.p.copy())
            St.append(self.S.copy())
        return np.array(xt), np.array(pt), np.array(St)