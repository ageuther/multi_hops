import numpy as np

class ElectronTransfer():
    def __init__(self, omega, alpha, delta, kappa, mass):
        self.mass = mass
        self.omega = omega
        self.alpha = alpha
        self.delta = delta
        self.kappa = kappa

    def potential(self, x):
        V0 = 0.5 * self.mass * self.omega**2 * x**2
        V = np.zeros((2, 2))
        V[0, 1] = V[1, 0] = self.delta
        V[0, 0] = self.alpha + self.kappa * x
        V[1, 1] = -V[0, 0]
        return V + V0 * np.eye(2)

    def gradient(self, x):
        G0 = self.mass * self.omega**2 * x
        G = np.zeros((2, 2))
        G[0, 0] = self.kappa
        G[1, 1] = -G[0, 0]
        # print(G0)
        return G + G0 * np.eye(2)
