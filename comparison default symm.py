import numpy as np
import matplotlib.pyplot as plt


# with np.load('default_tully1_xpS.npz') as f:
#     x = f['x']
#     p = f['p']
#     S = f['S']

# WPP_list = 2 * np.abs(S[:, 0, 2])

# with np.load('default_tully1_xpS_reverse.npz') as f2:
#     x2 = f['x2']
#     p2 = f['p2']
#     S2 = f['S2']

# WPP_list2 = 2 * np.abs(S2[:, 0, 2])
# diffp= p2[:, ::-1] - p

with np.load('symm_tully1_xpS.npz') as f_s:
    x_s = f_s['x']
    p_s = f_s['p']
    S_s = f_s['S']

WPP_list = 2 * np.abs(S_s[:, 0, 2])

with np.load('symm_tully1_xpS_reverse.npz') as f2_s:
    x2_s = f_s['x2']
    p2_s= f_s['p2']
    S2_s = f_s['S2']

WPP_list2 = 2 * np.abs(S2_s[:, 0, 2])



diffp_s= p2_s[:, ::-1] - p_s

fig = plt.figure(figsize=(15, 10))
plt.plot(diffp_s.T)
plt.plot(diffp.T)
# Show the plot
plt.show()