import numpy as np
import matplotlib.pyplot as plt
from mash import MASH
from sampler import sample_bloch, sample_p
from electron_transfer import ElectronTransfer
from polylib.PES.nonadiabatic import TwoLevelAdiabaticRepresentation
from polylib import units


pes = ElectronTransfer(1,0,10,1,1)
adiab = TwoLevelAdiabaticRepresentation(pes)
xs = np.linspace(-5, 5, 100)
Vs_diab = np.array([pes.potential(x) for x in xs])
Vs_adiab = np.array([adiab.potential(x) for x in xs])

# plt.plot(xs, Vs_adiab)
# plt.show()

t_total = 150e-15 / units.atomic.time 
dt = 0.005
dt1 = 0.1
steps = 2000

m = 1
num_samples = 1
WPP_list = []  # List to store weights for each iteration

x = []
p = []
S = []
e = []



for i in range(num_samples):
    
    x_init = -1
    p_init = 5
    S_init =[-0.86158784,  0.50743608, -0.01322918] #sample_bloch()
    
   
    S_init /= np.linalg.norm(S_init) # normalise because numerical method
    
    V = adiab.potential(x_init)
    Vz = (V[1] - V[0]) / 2
    Vbar = (V[1] + V[0]) / 2
    print (Vz)

    e_init = p_init**2/(2*m) + Vbar + Vz*np.sign(S_init[2])

    
    masher = MASH(adiab, dt, m)
    masher.set_xpS(x_init, p_init, S_init, e_init)
    WPP = 2 * np.abs(S_init[2])  # Weighting factor for histogram
    # calculates forward and reverse trajectory using symmetric method
    xts, pts, Sts, ets = masher.trajectory(steps, 'symmetric')
    x.append(xts)
    p.append(pts)
    S.append(Sts)
    e.append(ets)

    masher2 = MASH(adiab, dt1, m)
    masher2.set_xpS(xts[-1], pts[-1], Sts[-1], ets[-1])
    xts2, pts2, Sts2, ets2 = masher2.trajectory(steps, 'symmetric')
    x2.append(xts2)
    p2.append(pts2)
    S2.append(Sts2)
    e2.append(ets2)

   
    WPP_list.append(WPP)

x = np.array(x)
p = np.array(p)
S = np.array(S)
e = np.array(e)

p_final = p[:, -1]


fig = plt.figure(figsize=(15, 10))
plt.subplot(341)
plt.plot(p.T, color='black', alpha=0.5, label='forward')

plt.ylabel ('p')
plt.title('momentum symmetric method')
plt.legend()

plt.subplot(342)
plt.plot(S[:, :, 2].T, color='black', alpha=0.5)

plt.xlabel('time steps')
plt.ylabel('Sz')
plt.title('Spin symmetric method')

plt.subplot(343)  
plt.plot(x.T, color='black', alpha=0.5)

plt.xlabel('time steps')
plt.ylabel('q')
plt.title('q symmetric method')

# plt.hist(p_final, bins=1000, weights=WPP_list) # weight according to Sz
# plt.xlim(15, 25)
# plt.xlabel('time steps')
# plt.ylabel ('histogram momentum')

plt.subplot(344)
plt.plot(e.T, label='analytic', color='black', alpha=0.5)
plt.xlabel('time steps')
plt.ylabel ('energy')
plt.title('energy symmetric method')



plt.ylabel ('p')
plt.title('momentum asymmetric method')


plt.tight_layout()  # Adjust subplot spacing
plt.show() 

