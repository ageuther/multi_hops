import numpy as np
from scipy.linalg import expm
class MASH:
    
    def __init__(self, adiab, dt, m, bisect=2):
        self.adiab = adiab
        self.dt = dt
        self.m = m
        self.bisect = bisect

    def set_xpS(self, x, p, S, e):
        self.x, self.p, self.S, self.e = x, p, S, e
    
    def verlet(self, q, p, state):
        F = -self.adiab.gradient(q)[state]
        p += F*self.dt/(2)
        q += p*self.dt/self.m
        F = -self.adiab.gradient(q)[state]
        p += F*self.dt/(2) 
        return q, p
    
    def spin_update(self, q, p, S, method):
        Sx, Sy, Sz = S

        d = self.adiab.first_order_coupling(q)[0, 1]
        V = self.adiab.potential(q)
        Vz = (V[1] - V[0]) / 2

        if method == 'default':
            Sx_dot = 2 * d * p / self.m * Sz - 2 * Vz * Sy
            Sy_dot = 2 * Vz * Sx
            Sz_dot = -2 * d * p / self.m * Sx

            new_Sx = Sx + self.dt * Sx_dot
            new_Sy = Sy + self.dt * Sy_dot
            new_Sz = Sz + self.dt * Sz_dot

            return [new_Sx, new_Sy, new_Sz]
        
        elif method == 'analytic_reduced':
            a = 2 * d * p / self.m
            b = 2 * Vz
            coeff_S = np.array([[0, -b, a], [b, 0, 0], [-a, 0, 0]])  # Matrix for coefficients of System of ODEs

            # Compute the matrix exponential of coeff_S multiplied by self.dt
            exp_coeff_S_dt = expm(coeff_S * self.dt/2)

            # Update the spin using the matrix exponential
            new_S = np.dot(exp_coeff_S_dt, S)

            # Extract the components of the updated spin
            new_Sx, new_Sy, new_Sz = new_S

            return [new_Sx, new_Sy, new_Sz]
        
        

    
    def check_jumps(self,new_x, new_state, state, new_S, new_p):
        V = self.adiab.potential(new_x)
        #S_cross0 = 0
        if state != new_state:
            # S_cross0 = 1
            # if (dt > self.dt / 2**self.bisect):
            #    new_dt=dt/2
            # else:
                E1 = new_p**2 / (2 * self.m) + V[state]
                E2 = V[new_state]
                if E1 >= E2:
                    new_p = np.sign(new_p) * np.sqrt((E1 - E2) * 2 * self.m)
                    state = new_state
                    print('hopped')
                else:
                    new_p = -new_p
                    new_S[2] = -new_S[2]
                    print('not hopped')
        return state, new_p, new_S[2]



    def trajectory(self, steps, method):
        xts = [self.x]
        pts = [self.p]
        Sts = [self.S]
        ets = [self.e]
        
        state = int(self.S[2] > 0)
        with open('output.txt', 'a') as f:
     
            for i in range(1, steps):
                print('--------------')
                print('step ', i)
                old_x, old_p = xts[i-1], pts[i-1]
                old_S = Sts[i-1]
                if method == 'verlet,spin':
                    new_x, new_p = self.verlet(old_x, old_p, state)
                    
                    new_S = self.spin_update(new_x, new_p, old_S,'analytic_reduced') 
                    new_S = self.spin_update(new_x, new_p, new_S,'analytic_reduced') 
                    new_S = new_S / np.linalg.norm(new_S)
                    
                    V = self.adiab.potential(new_x)
                    Vz = (V[1] - V[0]) / 2
                    Vbar = (V[1] + V[0]) / 2
                    new_state = int(new_S[2]>0)
                    state, new_p, new_S[2] = self.check_jumps(new_x, new_state, state, new_S, new_p)
                elif method =='symmetric':
                    new_S = self.spin_update(old_x, old_p, old_S,'analytic_reduced') 
                    new_S = new_S / np.linalg.norm(new_S)
                    new_state = int(new_S[2]>0)

                    V = self.adiab.potential(old_x)

                    state, new_p, new_S[2] = self.check_jumps(old_x, new_state, state, new_S, old_p)
                    e = new_p**2/(2*self.m) + V[state]
                    print (e, file = f)

                    new_x, new_p = self.verlet(old_x, new_p, state)
                    
                    V = self.adiab.potential(new_x)
                    e = new_p**2/(2*self.m) + V[state]
                    print (e, file = f)

                    new_S = self.spin_update(new_x, new_p, new_S,'analytic_reduced')
                    new_S = new_S / np.linalg.norm(new_S)
                    new_state = int(new_S[2]>0)

                    state, new_p, new_S[2] = self.check_jumps(new_x, new_state, state, new_S, new_p)

                    e = new_p**2/(2*self.m) + V[state]
                    print (e, file = f)

                V = self.adiab.potential(new_x)
                new_e = new_p**2/(2*self.m) + V[state]
                

                xts.append(new_x)
                pts.append(new_p)
                Sts.append(new_S)
                ets.append(new_e)
                print (i, file = f)
                print ('___', file = f)


        return xts, pts, Sts, ets


