import numpy as np

def sample_bloch():
    theta = np.random.uniform(np.pi/2, np.pi)  # we need to be on lower hemisphere -> cos(theta) is <= 0 for theta in this range 
    phi = np.random.uniform(0, 2*np.pi)        # azimuthal
    state = np.array([np.sin(theta)*np.cos(phi), np.sin(theta)*np.sin(phi), np.cos(theta)]) #state construction for 3D vector 
    
    return state


def sample_p(gamma, p_bar):
     z = np.random.normal()
    # apply scale parameter gamma to the exponent
    # exponent = (-1 / gamma) * (p_bar - p)**2
    # invert cdf
     p = p_bar + z / np.sqrt(-2 * np.log(np.random.uniform(0, 1))) * np.sqrt(gamma)
     return p

# def sample_x(gamma, x_bar):
#     y = np.random.normal()
#     # apply scale parameter gamma to the exponent
#     # exponent = (-1 / gamma) * (p_bar - p)**2
#     # invert cdf
#     x = x_bar + y / np.sqrt(-2 * np.log(np.random.uniform(0, 1))) / np.sqrt(gamma)
#     return x

# from Wiegner distribution

