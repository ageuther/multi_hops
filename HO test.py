import numpy as np
import matplotlib.pyplot as plt
from mash import MASH
from sampler import sample_bloch, sample_p
from polylib.PES.diabatic import Tully, harmonic
from polylib.PES.nonadiabatic import TwoLevelAdiabaticRepresentation
from polylib import units


class HarmonicOscillator:
    def __init__(self, A=0.01, B=1.6, C=0.005, D=1.0):
        self.A = A
        self.B = B
        self.C = C
        self.D = D
        self.insty = 0
        self.ylim = None
        self.xlim = (-10, 10)
        self.mass = 2000.0
        self.Vr = -A

    def potential(self, x):
        V = np.empty((2, 2))
        V[0, 0] = 0.5 * self.mass * self.B**2 * x**2  # Harmonic oscillator potential
        V[1, 1] = -V[0, 0]  # Negative of the potential
        V[0, 1] = V[1, 0] = self.C * np.exp(-self.D * x**2)  # Off-diagonal elements
        return V

    def vpotential(self, x):
        N = len(x)
        V = np.empty((N, 2, 2))
        V[:, 0, 0] = 0.5 * self.mass * self.B**2 * x**2  # Harmonic oscillator potential
        V[:, 1, 1] = -V[:, 0, 0]  # Negative of the potential
        V[:, 0, 1] = V[:, 1, 0] = self.C * np.exp(-self.D * x**2)  # Off-diagonal elements
        return V

    def gradient(self, x):
        G = np.empty((2, 2))
        G[0, 0] = self.mass * self.B**2 * x  # Gradient of the harmonic oscillator potential
        G[1, 1] = - G[0, 0]  # Negative of the gradient
        G[0, 1] = G[1, 0] = -2 * self.C * self.D * x * np.exp(-self.D * x**2)  # Off-diagonal elements
        return G

    def hessian(self, x):
        H = np.empty((2, 2))
        H[0, 0] = self.mass * self.B**2  # Second derivative of the harmonic oscillator potential
        H[1, 1] = - H[0, 0]  # Negative of the second derivative
        H[0, 1] = H[1, 0] = 2 * self.C * self.D * (1 - 2 * self.D * x**2) * np.exp(-self.D * x**2)  # Off-diagonal elements
        return H



pes = harmonic()
adiab = TwoLevelAdiabaticRepresentation(pes)
xs = np.linspace(-5, 5, 100)
Vs_diab = np.array([pes.potential(x) for x in xs])
Vs_adiab = np.array([adiab.potential(x) for x in xs])

# plt.plot(xs, Vs_diab[:, 0, 0])
# plt.plot(xs, Vs_diab[:, 1, 1])
# plt.plot(xs, Vs_adiab[:, 0])
# plt.plot(xs, Vs_adiab[:, 1])
# plt.show()

t_total = 150e-15 / units.atomic.time
dt = 1 # accuracy 
steps = 10
# steps = t_total // dt

m = 2000
num_samples = 10
WPP_list = []  # List to store weights for each iteration

x = []
p = []
S = []
for i in range(num_samples):
    
    x_init = -4 #sample_x(0.1,)
    p_init = sample_p(0.1,np.sqrt(2*m*0.1))
    S_init = sample_bloch()
   
    S_init /= np.linalg.norm(S_init) # normalise because numerical method
    
    
    
    masher = MASH(adiab, dt, m)
    masher.set_xpS(x_init, p_init, S_init)
    WPP = 2 * np.abs(S_init[2])  # Weighting factor for histogram
    xts, pts, Sts = masher.trajectory(S_init, steps)
    x.append(xts)
    p.append(pts)
    S.append(Sts)
    WPP_list.append(WPP)

x = np.array(x)
p = np.array(p)
S = np.array(S)


np.savez('tully1_xpS.npz', x=x, p=p, S=S)

with np.load('tully1_xpS.npz') as f:
    x = f['x']
    p = f['p']
    S = f['S']
p_final = p[:, -1]
WPP_list = 2 * np.abs(S[:, 0, 2])

fig = plt.figure(figsize=(15, 5))
# plt.subplot(131)
# plt.plot(x.T)
plt.subplot(132)
plt.plot(p.T)
plt.subplot(131)






plt.plot(S[:, :, 2].T)

# bin_widthx = 3.5 * np.std(x_flat) / (len(x_flat) ** (1/3))
# num_binsx = int((max(x_flat) - min(x_flat)) / bin_widthx)

# plt.subplot(131) 
# plt.hist(x_flat, bins=num_binsx)
bin_widthp = 3.5 * np.std(p_final) / (len(p_final) ** (1/3))
num_binsp = int((max(p_final) - min(p_final)) / bin_widthp)
plt.subplot(133)  
plt.hist(p_final, bins=1000, weights=WPP_list) # weight according to Sz
plt.xlim(15, 25)

# plt.tight_layout()  # Adjust subplot spacing
plt.show()  # Display the plot

